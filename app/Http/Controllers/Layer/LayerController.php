<?php

namespace App\Http\Controllers\Layer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Layers\LayerDestoryRequest;
use App\Http\Requests\Layers\LayerPatchRequest;
use App\Http\Requests\Layers\LayerShowRequest;
use App\Http\Requests\Layers\LayerStoreRequest;
use App\Services\Layer\LayerServiceInterface;
use Illuminate\Http\Request;

class LayerController extends Controller
{

    private $layerService;

    public function __construct(LayerServiceInterface $layerService)
    {
        $this->layerService = $layerService;
    }

    public function index() {
        return $this->layerService->getAllLayers();
    }

    public function store(LayerStoreRequest $request)
    {
        $payload = $request->only(
            [
                'data',
                'level'
            ]
        );
        return $this->layerService->storeLayer($payload);
    }

    public function show(LayerShowRequest $request,$id)
    {
        return $this->layerService->showLayer($id);
    }

    public function update(LayerPatchRequest $request,$id)
    {
        $payload = $request->only(
            [
                'data',
                'level',
            ]
        );
        return $this->layerService->patchLayer($id,$payload);
    }

    public function destroy(LayerDestoryRequest $request, $id)
    {
        return $this->layerService->destroyLayer($id);
    }

}
