<?php

namespace App\Http\Requests\Layers;

use Illuminate\Foundation\Http\FormRequest;

class LayerPatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge(
            [
                'id'=>$this->route('id'),
            ]
        );
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */


    public function rules()
    {
        $rules = [
            'id'=>'required|integer|exists:layers,id',
        ];

        if ($this->has('data')) {
            $rules['data'] = 'required|json';
        }
        if ($this->has('level')) {
            $rules['level'] = 'required|integer|max:9000';
        }
        return $rules;
    }


}
