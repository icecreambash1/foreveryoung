<?php

namespace App\Providers;

use App\Repositories\Layer\LayerRepository;
use App\Repositories\Layer\LayerRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class LayerRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LayerRepositoryInterface::class,
            LayerRepository::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
