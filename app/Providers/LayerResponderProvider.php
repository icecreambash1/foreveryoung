<?php

namespace App\Providers;

use App\Responders\Layer\LayerResponder;
use App\Responders\Layer\LayerResponderInterface;
use Illuminate\Support\ServiceProvider;

class LayerResponderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LayerResponderInterface::class,
            LayerResponder::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
