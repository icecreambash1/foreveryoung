<?php

namespace App\Providers;

use App\Services\Layer\LayerService;
use App\Services\Layer\LayerServiceInterface;
use Illuminate\Support\ServiceProvider;

class LayerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LayerServiceInterface::class,
            LayerService::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
