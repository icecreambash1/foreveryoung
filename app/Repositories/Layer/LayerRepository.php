<?php

namespace App\Repositories\Layer;

use App\Models\Layer;

class LayerRepository implements LayerRepositoryInterface
{

    public function all()
    {
        return Layer::all();
    }

    public function getByLayer(Layer $layer)
    {
        return $layer->where('id','=',$layer->id)->first();
    }

}
