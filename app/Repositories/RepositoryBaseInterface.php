<?php

namespace App\Repositories;

use App\Models\Layer;

interface RepositoryBaseInterface
{
    public function all();

    public function getByLayer(Layer $layer);
}
