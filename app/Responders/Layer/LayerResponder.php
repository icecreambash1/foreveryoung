<?php

namespace App\Responders\Layer;

class LayerResponder implements LayerResponderInterface
{

    public function respond()
    {
        // TODO: Implement respond() method.
    }

    public function getAllUserRespond($data)
    {
        return response($data,200);
    }

    public function storeLayerRespond($data)
    {
        if($data) {
            return response($data,201);
        }
        return response('Error', 409);

    }
    public function showLayerRespond($data) {
        return response($data,200);
    }

    public function destoyLayerRespond($data)
    {
        return response(null,204);
    }

    public function patchLayerRespond($data)
    {
        return response(null,204);
    }
}
