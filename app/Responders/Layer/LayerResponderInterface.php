<?php

namespace App\Responders\Layer;

use App\Responders\ResponderBaseInterface;

interface LayerResponderInterface extends ResponderBaseInterface
{
    public function getAllUserRespond($data);
    public function storeLayerRespond($data);
    public function showLayerRespond($data);
    public function destoyLayerRespond($data);
    public function patchLayerRespond($data);
}
