<?php

namespace App\Responders;

interface ResponderBaseInterface
{
    public function respond();
}
