<?php

namespace App\Services\Layer;

use App\Models\Layer;
use App\Repositories\Layer\LayerRepositoryInterface;
use App\Responders\Layer\LayerResponderInterface;

class LayerService implements LayerServiceInterface
{

    private $layerRepository;
    private $layerResponder;

    public function __construct(LayerRepositoryInterface $layerRepository, LayerResponderInterface $layerResponder)
    {
        $this->layerRepository = $layerRepository;
        $this->layerResponder = $layerResponder;
    }

    public function getAllLayers()
    {
        return $this->layerResponder->getAllUserRespond(
            $this->layerRepository->all()
        );
    }

    public function storeLayer($data)
    {

        return $this->layerResponder->storeLayerRespond(
            Layer::create($data)
        );
    }

    public function showLayer($id)
    {
        return $this->layerResponder->showLayerRespond(
            $this->layerRepository->getByLayer(Layer::find($id))
        );
    }

    public function destroyLayer($id)
    {
        return $this->layerResponder->destoyLayerRespond(
            Layer::destroy($id),
        );
    }

    public function patchLayer($id, $payload)
    {
        return $this->layerResponder->patchLayerRespond(
            Layer::whereId($id)->update($payload)
        );
    }
}
