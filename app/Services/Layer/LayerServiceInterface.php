<?php

namespace App\Services\Layer;

interface LayerServiceInterface
{
    public function getAllLayers();
    public function storeLayer($data);
    public function showLayer($id);
    public function destroyLayer($id);
    public function patchLayer($id, $payload);
}
