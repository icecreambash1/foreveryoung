<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Layer>
 */
class LayerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'level'=>$this->faker->randomDigit(),
            'data'=> json_encode(
                [
                    $this->faker->randomElement(
                        [
                            "test",
                            "point",
                            "die",
                            "forever",
                            "dude",
                        ]
                    )
                ]
            )
        ];
    }
}
